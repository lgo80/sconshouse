<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|image|max:6048',
        ]);
        if ($request->file('file')) {
            $images = $request->file('file');
            foreach ($images as $image) {
                $url = Storage::put('Products', $image);
                Image::create([
                    'url' => $url,
                    'imageable_id' => 2,
                    'imageable_type' => Product::class
                ]);
            }
        }

        /*if ($request->file('file')) {
            $images = $request->file('file');
            $product = $request->file('product');
            foreach ($images as $image) {
                $url = Storage::put('Products', $image);
                $product->imageses()->create([
                    'url' => $url
                ]);
            }
        }*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Image  $image
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image, Product $product)
    {

        return $image->product;
        $image->delete();
        return redirect()
            ->route('admin.products.edit', compact('product'))
            ->with('info', 'La imagen del producto se borro con éxito');
    }
}
