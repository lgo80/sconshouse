<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductControlador extends Controller
{

    public function __construct()
    {
        $this->middleware('can:admin.products.index')->only('index');
        $this->middleware('can:admin.products.edit')->only('edit', 'update');
        $this->middleware('can:admin.products.create')->only('create', 'store');
        $this->middleware('can:admin.products.destroy')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'slug' => 'required|unique:products',
            'review' => 'required',
            'description' => 'required',
            'price' => 'required',
            'file' => 'required',
            'file.*' => 'image',
        ]);

        $product = Product::create($request->all());

        $images = $request->file('file');

        $this->grabarImagen($images, $product);

        return redirect()
            ->route('admin.products.edit', compact('product'))
            ->with('info', 'El producto se creó con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required|min:3',
            'slug' => "required|unique:products,slug,$product->id",
            'review' => 'required',
            'description' => 'required',
            'price' => 'required',
            'file.*' => 'required|image',
        ]);

        $product->update($request->all());

        $images = $request->file('file');

        $this->grabarImagen($images, $product);

        return redirect()
            ->route('admin.products.edit', compact('product'))
            ->with('info', 'El producto se modificó con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (isset($product->imageses)) {
            foreach ($product->imageses as $image) {
                $image->delete();
                Storage::delete($image->url);
            }
        }

        $product->delete();

        return redirect()
            ->route('admin.products.index')
            ->with('info', 'El producto se borro con éxito');
    }

    public function grabarImagen($images, Product $product)
    {
        if ($images) {
            foreach ($images as $image) {
                $name = Str::random(10) . $image->getClientOriginalName();
                $ruta = storage_path() . '\app\public\products/' . $name;

                Image::make($image)
                    ->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save($ruta);

                $product->imageses()->create([
                    'url' => 'products/' . $name
                ]);
            }
        }
    }
}
