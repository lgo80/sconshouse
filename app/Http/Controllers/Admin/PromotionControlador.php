<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Http\Request;

class PromotionControlador extends Controller
{

    public function __construct()
    {
        $this->middleware('can:admin.promotions.index')->only('index');
        $this->middleware('can:admin.promotions.edit')->only('edit', 'update');
        $this->middleware('can:admin.promotions.create')->only('create', 'store');
        $this->middleware('can:admin.promotions.destroy')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::all();
        return view('admin.promotions.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::pluck('name', 'id');
        return view('admin.promotions.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'slug' => 'required|unique:products',
            'review' => 'required',
            'description' => 'required',
            'product_id' => 'required',
            'count' => 'required',
            'price' => 'required',
        ]);

        $promotion = Promotion::create($request->all());

        return redirect()
            ->route('admin.promotions.edit', compact('promotion'))
            ->with('info', 'El producto se creó con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        $products = Product::pluck('name', 'id');
        return view('admin.promotions.edit', compact('promotion', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $request->validate([
            'name' => 'required|min:3',
            'slug' => 'required|unique:products',
            'review' => 'required',
            'description' => 'required',
            'product_id' => 'required',
            'count' => 'required',
            'price' => 'required',
        ]);

        $promotion->update($request->all());

        return redirect()
            ->route('admin.promotions.edit', compact('promotion'))
            ->with('info', 'El producto se modificó con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Promotion $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        $promotion->delete();

        return redirect()
            ->route('admin.promotions.index')
            ->with('info', 'La promoción se borro con éxito');
    }
}
