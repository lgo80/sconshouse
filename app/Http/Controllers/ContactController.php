<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContact;
use App\Mail\ContactMaileable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Mustra el home de la pagina
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('contact.create');
    }

    public function store(StoreContact $request)
    {
        $correo = new ContactMaileable($request->all());

        Mail::to('lgo80@yahoo.com.ar')->send($correo);

        return redirect()->route('contact.create')
            ->with(
                'info',
                'La consulta fue enviada correctamente'
            );
    }
}
