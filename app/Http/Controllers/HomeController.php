<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Mustra el home de la pagina
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        // $products = Product::all();
        // $products = Product::has('comments', '>=', 3)->get();
        //return $products[0]->commentsOrderByIdDesc;
        // return $products[0]->liked;

        /*return Product::whereHas('likes', function (Builder $query) {
            $query->where('user_id', 2);
        })->get();*/

        //return $products[3]->liked();
        return view('home.index');
    }
}
