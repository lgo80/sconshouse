<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Mustra el home de la pagina
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $promotions = Promotion::all()->random(3);
        return view('product.index', compact('promotions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $like
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }
}
