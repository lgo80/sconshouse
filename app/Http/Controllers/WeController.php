<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeController extends Controller
{
    /**
     * Mustra el home de la pagina
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('we.index');
    }
}
