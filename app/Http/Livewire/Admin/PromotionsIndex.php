<?php

namespace App\Http\Livewire\Admin;

use App\Models\Promotion;
use Livewire\Component;
use Livewire\WithPagination;

class PromotionsIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {

        $promotions = Promotion::where('name', 'LIKE', '%' . $this->search . '%')
            ->latest('id')
            ->paginate();
        return view('livewire.admin.promotions-index', compact('promotions'));
    }
}
