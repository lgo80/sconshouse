<?php

namespace App\Http\Livewire;

use App\Models\Image;
use Livewire\Component;
use Illuminate\Support\Facades\Storage;

class ImagesProducts extends Component
{

    public $product;

    public function render()
    {
        return view('livewire.images-products');
    }

    public function destroy(Image $image)
    {
        $image->delete();
        Storage::delete($image->url);
        $this->product->refresh();
    }
}
