<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Product;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;

class LikeComment extends Component
{

    use WithPagination;
    public $commentNew;

    protected $rules = [
        'commentNew' => 'required|min:3'
    ];

    protected $validationAttributes = [
        'commentNew' => 'comentario'
    ];

    public function render(Request $request)
    {
        $products = Product::paginate();
        return view('livewire.like-comment', compact('products'));
    }

    public function modifyILikeProduct($id, $isLiked)
    {
        if ($isLiked) {
            $this->getDestroy($id, Product::class);
        } else if (auth()->user()) {
            $this->getStore($id, Product::class);
        }
    }

    public function modifyILikeComment(Comment $comment)
    {

        if (count($comment->liked())) {
            $this->getDestroy($comment->id, Comment::class);
        } else if (auth()->user()) {
            $this->getStore($comment->id, Comment::class);
        }
    }

    private function getDestroy($id, $class)
    {
        $like =  Like::where('user_id', auth()->user()->id)
            ->where('likeable_id', $id)
            ->where('likeable_type', $class);
        $like->delete();
    }

    private function getStore($id, $class)
    {
        Like::create([
            'likeable_id' => $id,
            'likeable_type' => $class,
            'user_id' => auth()->user()->id
        ]);
    }

    public function saveComment($id)
    {
        if (auth()->user()) {

            $this->validate();

            Comment::create([
                'comment' => $this->commentNew,
                'commentable_id' => $id,
                'commentable_type' => Product::class,
                'user_id' => auth()->user()->id
            ]);

            $this->reset('commentNew');
        }
    }
}
