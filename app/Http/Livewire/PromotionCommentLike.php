<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Promotion;
use Livewire\Component;

class PromotionCommentLike extends Component
{
    public Promotion $promotion;
    public $commentNew;

    protected $rules = [
        'commentNew' => 'required|min:3'
    ];

    protected $validationAttributes = [
        'commentNew' => 'comentario'
    ];

    public function render()
    {
        return view('livewire.promotion-comment-like');
    }

    public function modifyILikePromotion($id, $isLiked)
    {
        if ($isLiked) {
            $this->getDestroy($id, Promotion::class);
        } else if (auth()->user()) {
            $this->getStore($id, Promotion::class);
        }
    }

    public function modifyILikeComment(Comment $comment)
    {

        if (count($comment->liked())) {
            $this->getDestroy($comment->id, Comment::class);
        } else if (auth()->user()) {
            $this->getStore($comment->id, Comment::class);
        }
    }

    private function getDestroy($id, $class)
    {
        $like =  Like::where('user_id', auth()->user()->id)
            ->where('likeable_id', $id)
            ->where('likeable_type', $class);
        $like->delete();
        $this->promotion->refresh();
    }

    private function getStore($id, $class)
    {
        Like::create([
            'likeable_id' => $id,
            'likeable_type' => $class,
            'user_id' => auth()->user()->id
        ]);
        $this->promotion->refresh();
    }

    public function saveComment($id)
    {
        if (auth()->user()) {

            $this->validate();

            Comment::create([
                'comment' => $this->commentNew,
                'commentable_id' => $id,
                'commentable_type' => Promotion::class,
                'user_id' => auth()->user()->id
            ]);

            $this->reset('commentNew');
            $this->promotion->refresh();
        }
    }
}
