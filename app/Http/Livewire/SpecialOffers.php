<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Promotion;
use Livewire\Component;

class SpecialOffers extends Component
{
    public $promotions;
    public $commentNew;
    private $idPromotion;

    protected $rules = [
        'commentNew' => 'required|min:3'
    ];

    protected $validationAttributes = [
        'commentNew' => 'comentario'
    ];

    public function render()
    {
        return view('livewire.special-offers');
    }

    public function modifyILikePromotion($id, $isLiked)
    {

        $this->idPromotion = $id;
        if ($isLiked) {
            $this->getDestroy($id, Promotion::class);
        } else if (auth()->user()) {
            $this->getStore($id, Promotion::class);
        }
    }

    public function modifyILikeComment(Comment $comment, $idPromotion)
    {

        $this->idPromotion = $idPromotion;
        if (count($comment->liked())) {
            $this->getDestroy($comment->id, Comment::class);
        } else if (auth()->user()) {
            $this->getStore($comment->id, Comment::class);
        }
    }

    public function getDestroy($id, $class)
    {
        $like =  Like::where('user_id', auth()->user()->id)
            ->where('likeable_id', $id)
            ->where('likeable_type', $class);
        $like->delete();
        $this->promotions->find($this->idPromotion)->refresh();
    }

    public function getStore($id, $class)
    {
        Like::create([
            'likeable_id' => $id,
            'likeable_type' => $class,
            'user_id' => auth()->user()->id
        ]);
        $this->promotions->find($this->idPromotion)->refresh();
    }

    public function saveComment($id)
    {
        if (auth()->user()) {

            $this->validate();

            Comment::create([
                'comment' => $this->commentNew,
                'commentable_id' => $id,
                'commentable_type' => Promotion::class,
                'user_id' => auth()->user()->id
            ]);

            $this->reset('commentNew');
            $this->promotions->find($id)->refresh();
        }
    }
}
