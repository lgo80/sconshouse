<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Like;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Relacion muchos a uno
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacion polimorfica
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function liked()
    {
        $idUser = (auth()->user()) ? auth()->user()->id : 0;
        return $this->likes()->where('user_id', $idUser)->get();
    }
}
