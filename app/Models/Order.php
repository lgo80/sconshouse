<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shipping;
use App\Models\Payment;
use App\Models\User;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Relacion muchos a uno
     */
    public function shipping()
    {
        return $this->belongsTo(Shipping::class);
    }

    /**
     * Relacion muchos a uno
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * Relacion muchos a uno
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
