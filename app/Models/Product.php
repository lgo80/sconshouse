<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Image;
use App\Models\Promotion;
use App\Models\OrderDetail;
use App\Models\Comment;
use App\Models\Like;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Relacion uno a muchos polimorfica
     */
    public function imageses()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * Relacion uno a muchos
     */
    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function orderDetails()
    {
        return $this->morphMany(OrderDetail::class, 'orderDetailable');
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function commentsOrderByIdDesc()
    {
        return $this->comments()->orderBy('id', 'desc');
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function liked()
    {
        $idUser = (auth()->user()) ? auth()->user()->id : 0;
        return $this->likes()->where('user_id', $idUser)->get();
    }

    public function promotionsByThree()
    {
        $Promotions = $this->promotions()->get();
        return (count($Promotions) > 3) ? $Promotions->random(3) : $Promotions;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
