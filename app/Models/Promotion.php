<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\Comment;
use App\Models\Like;

class Promotion extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Relacion muchos a uno
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function orderDetails()
    {
        return $this->morphMany(OrderDetail::class, 'orderDetailable');
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function liked()
    {
        $idUser = (auth()->user()) ? auth()->user()->id : 0;
        return $this->likes()->where('user_id', $idUser)->get();
    }

    /**
     * Relacion uno a muchos polimorfica
     */
    public function commentsOrderByIdDesc()
    {
        return $this->comments()->orderBy('id', 'desc');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
