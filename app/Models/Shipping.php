<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;

class Shipping extends Model
{
    use HasFactory;

    protected $fillable =  ['detail'];
    
    /**
     * Relacion uno a muchos
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
