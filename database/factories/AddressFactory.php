<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'street' => $this->faker->sentence(),
            'number' => rand(1, 6000),
            'floor' => rand(1, 9),
            'department' => Str::random(2),
            'location' => $this->faker->randomElement(['CABA', 'Gran Buenos Aires']),
            'zip_code' => rand(1, 9999),
        ];
    }
}
