<?php

namespace Database\Factories;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /* $table->string();
            $table->double('');
            $table->dateTime('');
         */
        return [
            'type' => $this->faker->randomElement(["Efectivo", "Tarjeta"]),
            'amount' => rand(100, 3000),
            'date_at' => $this->faker->dateTime(),
        ];
    }
}
