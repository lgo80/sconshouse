<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PromotionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Promotion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->word(15);
        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'count' => rand(1, 20),
            'price' => rand(100, 600),
            'review' => $this->faker->text(60),
            'Description' => $this->faker->paragraph(),
            'product_id' => Product::all()->random()->id,
        ];
    }
}
