<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Payment;
use App\Models\Shipping;
use Database\Factories\AddressFactory;
use Database\Factories\RoleFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('pictures');;
        Storage::makeDirectory('pictures');

        Address::factory(12)->create();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        Shipping::factory(14)->create();
        Payment::factory(14)->create();
        $this->call(ProductSeeder::class);
        $this->call(PromotionSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(LikeSeeder::class);
    }
}
