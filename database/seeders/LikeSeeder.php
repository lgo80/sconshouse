<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Like;
use Illuminate\Database\Seeder;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comments = Comment::all();

        foreach ($comments as $comment) {
            Like::factory(rand(0, 20))->create([
                'likeable_id' => $comment->id,
                'likeable_type' => Comment::class
            ]);
        }
    }
}
