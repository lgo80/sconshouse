<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::factory(18)->create();

        foreach ($orders as $order) {
            $totalDetalle = rand(1, 4);
            for ($i = 1; $i <= $totalDetalle; $i++) {
                $cant = rand(0, 1);
                OrderDetail::factory(1)->create([
                    'order_detailable_id' => ($cant)
                        ? Product::all()->random()->id
                        : Promotion::all()->random()->id,
                    'order_detailable_type' => ($cant)
                        ? Product::class
                        : Promotion::class,
                    'order_id' => $order->id
                ]);
            }
        }
    }
}
