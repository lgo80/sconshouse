<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Image;
use App\Models\Like;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Product::factory(1)->create([
            'name' => 'Scons clásico',
            'slug' => 'scons-clasico'
        ]);

        Product::factory(1)->create([
            'name' => 'Scons con queso parmesano',
            'slug' => 'scons-con-queso-parmesano'
        ]);

        Product::factory(1)->create([
            'name' => 'Scons con aceitunas',
            'slug' => 'scons-con-aceitunas',
        ]);

        Product::factory(1)->create([
            'name' => 'Scons con salame',
            'slug' => 'scons-con-salame',
        ]);

        $products = Product::all();

        foreach ($products as $product) {
            Image::factory(1)->create([
                'imageable_id' => $product->id,
                'imageable_type' => Product::class
            ]);
            Comment::factory(rand(0, 8))->create([
                'commentable_id' => $product->id,
                'commentable_type' => Product::class
            ]);
            Like::factory(rand(0, 20))->create([
                'likeable_id' => $product->id,
                'likeable_type' => Product::class
            ]);
        }
    }
}
