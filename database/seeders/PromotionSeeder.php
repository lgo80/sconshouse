<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Promotion;
use Illuminate\Database\Seeder;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promotion = Promotion::factory(18)->create();

        foreach ($promotion as $promotion) {
            Comment::factory(rand(0, 8))->create([
                'commentable_id' => $promotion->id,
                'commentable_type' => Promotion::class
            ]);

            Like::factory(rand(0, 20))->create([
                'likeable_id' => $promotion->id,
                'likeable_type' => Promotion::class
            ]);
        }
    }
}
