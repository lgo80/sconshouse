<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'Admin']);
        $role2 = Role::create(['name' => 'Blogger']);

        Permission::create(['name' => 'admin.home'])
            ->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.products.index'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.products.create'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.products.edit'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.products.destroy'])
            ->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.promotions.index'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.promotions.create'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.promotions.edit'])
            ->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.promotions.destroy'])
            ->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.users.index'])
            ->assignRole($role1);
        Permission::create(['name' => 'admin.users.create'])
            ->assignRole($role1);
        Permission::create(['name' => 'admin.users.edit'])
            ->assignRole($role1);
        Permission::create(['name' => 'admin.users.destroy'])
            ->assignRole($role1);
    }
}
