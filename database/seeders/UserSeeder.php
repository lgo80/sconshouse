<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Leonardo Gabriel Olmedo',
            'email' => 'lgo80@yahoo.com.ar',
            'password' => bcrypt('Boca2018'),
            'email_verified_at' => now(),
            'phone' => strval(rand(45000505, 69999999)),
            'mobile_phone' => strval(rand(1545000505, 1569999999)),
            'address_id' => Address::all()->random()->id,
            'remember_token' => Str::random(10),
        ])->assignRole('Admin');

        User::factory(19)->create();
    }
}
