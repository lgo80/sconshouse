@extends('adminlte::page')

@section('title', 'SconsHouse')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css"
        crossorigin="anonymous">
@endsection

@section('content_header')
    <h1>{{ __('messages.edit_product') }}</h1>
@stop

@section('content')

    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            {!! Form::model($product, ['route' => ['admin.products.update', $product], 'autocomplete' => 'off', 'files' => true, 'method' => 'put']) !!}
            @include('admin.products.partials.form')
            {!! Form::submit(__('messages.upgrade_product'), ['class' => 'btn btn-primary btn-sm']) !!}
            {!! Form::close() !!}
            <hr />
            {{-- <div class="card card-warning">
                <div class="card-header">
                    <h5>{{ __('messages.upload_new_photos') }}</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.images.store') }}" class="dropzone" id="my-awesome-dropzone"
                        method="POST">
                    </form>
                </div>
            </div> --}}
        </div>
    </div>
@stop

@section('css')
@stop
@section('js')
    <script src='{{ asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js') }} '></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#name").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
        });
        ClassicEditor
            .create(document.querySelector('#review'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
        //Cambiar imagen
        /*document.getElementById("file").addEventListener('change', cambiarImagen);

        function cambiarImagen(event) {
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }*/
        Dropzone.options.myAwesomeDropzone = {
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            /*paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }*/
        };

    </script>
@stop
