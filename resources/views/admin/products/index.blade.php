@extends('adminlte::page')

@section('title', 'SconsHouse')

@section('content_header')
    <h1 class="text-center">
        {{ trans_choice('messages.products', 2) }}
        @can('admin.products.create')
            <a class="btn btn-secondary btm-sm float-right" href="{{ route('admin.products.create') }}">
                {{ __('messages.new_product') }}
            </a>
        @endcan
    </h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    @livewire('admin.products-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
