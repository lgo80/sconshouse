<div class="form-group">
    {!! Form::label('name', __('messages.name')) !!}
    {!! Form::text('name', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_name') . '...',
]) !!}
    @error('name')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    <hr />
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_slug') . '...',
    'readonly',
]) !!}
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    <hr />
</div>
<div class="form-group">
    {!! Form::label('review', __('messages.review')) !!}
    {!! Form::textarea('review', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_review') . '...',
]) !!}
    @error('review')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    <hr />
</div>
<div class="form-group">
    {!! Form::label('description', __('messages.description') . ': ') !!}
    {!! Form::textarea('description', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_description') . '...',
]) !!}
    @error('description')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    <hr />
</div>
<div class="form-group">
    {!! Form::label('price', __('messages.price') . ': ') !!}
    {!! Form::number('price', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_price') . '...',
]) !!}
    @error('price')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    <hr />
</div>
<div class="card card-warning w-full">
    <div class="card-header">
        {!! Form::label('file', 'Imagen que se mostrara en el post', ['class' => 'card-title']) !!}
    </div>
    <div id="lala" class="card-body">

        @isset($product->imageses)
            @livewire('images-products',['product' => $product])
        @else
            <div class="card-columns">
                <div class="card">
                    <img id="picture0" class="card-img-top" src="{{ asset('images/no_disponible.jpg') }}" alt="">
                </div>
            </div>
        @endisset


    </div>
    <div class="card-footer">
        <div class="form-group">
            {!! Form::file('file[]', ['class' => 'form-control-file', 'accept' => 'image/*', 'multiple' => true]) !!}
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ab dolorum quod labore obcaecati odio
                necessitatibus! Id tempore aliquam labore magnam officia quod, vitae, ut voluptate, veniam harum sed
                eaque?</p>
        </div>
        @error('file')
            <small class="text-danger">{{ $message }}</small>
        @enderror
        @error('file.*')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<hr />
