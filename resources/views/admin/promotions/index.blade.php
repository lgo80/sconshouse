@extends('adminlte::page')

@section('title', 'SconsHouse')

@section('content_header')
    <h1 class="text-center">
        {{ trans_choice('messages.promotion', 2) }}
        @can('admin.promotions.create')
            <a class="btn btn-secondary btm-sm float-right" href="{{ route('admin.promotions.create') }}">
                {{ __('messages.new_promotion') }}
            </a>
        @endcan
    </h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    @livewire('admin.promotions-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
