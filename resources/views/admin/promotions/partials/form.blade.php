<div class="form-group">
    {!! Form::label('name', __('messages.name')) !!}
    {!! Form::text('name', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_name') . '...',
]) !!}

    <hr />
    @error('name')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_slug') . '...',
    'readonly',
]) !!}
    <hr />
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('review', __('messages.review')) !!}
    {!! Form::textarea('review', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_review') . '...',
]) !!}
    <hr />
    @error('review')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('description', __('messages.description') . ': ') !!}
    {!! Form::textarea('description', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_description') . '...',
]) !!}
    <hr />
    @error('description')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('product_id', __('messages.product_of_promotion') . ': ') !!}
    {!! Form::select('product_id', $products, null, [
    'class' => 'form-control',
]) !!}
    <hr />
    @error('product_id')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('count', __('messages.count') . ': ') !!}
    {!! Form::number('count', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_amount') . '...',
]) !!}
    <hr />
    @error('count')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('price', 'Precio: ') !!}
    {!! Form::number('price', null, [
    'class' => 'form-control',
    'placeholder' => __('messages.enter_price') . '...',
]) !!}
    <hr />
    @error('price')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
