@extends('adminlte::page')

@section('title', 'SconsHouse')

@section('content_header')
    <h1 class="text-center">Usuarios</h1>
@stop

@section('content')
    @livewire('admin.users-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
