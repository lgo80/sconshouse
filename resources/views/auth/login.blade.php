<x-app-layout>
    @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('login') }}" class="formUser">
        @csrf
        <x-jet-validation-errors class="mb-4" />

        <div>
            <x-jet-label for="email" value="{{ __('messages.email') }}" /> {{-- {{ __('Email') }} --}}
            <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required
                autofocus placeholder="{{ __('messages.enter_email') }}..." />
        </div>

        <div class="mt-4">
            <x-jet-label for="password" value="{{ __('messages.password') }}" />
            <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required 
            placeholder="{{ __('messages.enter_password') }}..." autocomplete="current-password" />
        </div>

        <div class="block mt-4">
            <label for="remember_me" class="flex items-center">
                <x-jet-checkbox id="remember_me" name="remember" />
                <span class="ml-2 text-sm text-gray-600">{{ __('messages.remember_me') }}</span>
            </label>
        </div>

        <div class="flex items-center justify-end mt-4">
            @if (Route::has('password.request'))
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
            @endif

            <x-jet-button class="ml-4">
                {{ __('Login') }}
            </x-jet-button>
        </div>
    </form>
</x-app-layout>
