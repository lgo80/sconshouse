<x-app-layout>
    <div class="body-primary">
        <div class="body-secundary xl:px-96">
            <div class="body-header-main">
                <p class="body-header-text">
                    {{ __('messages.contact_us') }}
                </p>
            </div>

            <div class="Structure-idx-primary">
                <dl class="nivel5">
                    <div class="flex">
                        <div class="ml-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="px-6">
                                        <strong>{{ __('messages.Leave_a_comment') }}</strong>
                                    </h2>
                                </div>
                                <div class="card-body">
                                    {!! Form::open(['route' => 'contact.store']) !!}
                                    <div class="">
                                        <div class="px-4 py-5 bg-white sm:p-6">
                                            <div class="grid grid-cols-6 gap-6">
                                                <div class="col-span-6">
                                                    {!! 
                                                    Form::label('name', __('messages.name') . ':', ['class' => 'block text-sm font-medium text-gray-700'])
                                                    !!}
                                                    {!! 
                                                    Form::text('name', null, [
                                                        'class' => 'mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md',
                                                        'autocomplete'=> "family-name",
                                                        'placeholder' => __('messages.enter_your_name') . '...'
                                                        ]) 
                                                    !!}
                                                    @error('name')
                                                        <span class="text-red-500">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-span-6">
                                                    {!! 
                                                    Form::label('email', __('messages.email') . ':', ['class' => 'block text-sm font-medium text-gray-700'])
                                                    !!}
                                                    {!! 
                                                    Form::text('email', null, [
                                                        'class' => 'mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md',
                                                        'autocomplete'=> "email",
                                                        'placeholder' => __('messages.enter_your_email') . '...'
                                                        ]) 
                                                    !!}
                                                    @error('email')
                                                        <span class="text-red-500">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-span-6">
                                                    {!! 
                                                    Form::label('subject', __('messages.subject') . ':', ['class' => 'block text-sm font-medium text-gray-700'])
                                                    !!}
                                                    {!! 
                                                    Form::text('subject', null, [
                                                        'class' => 'mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md',
                                                        'autocomplete'=> "subject",
                                                        'placeholder' => __('messages.enter_your_subject') . '...'
                                                        ]) 
                                                    !!}
                                                    @error('subject')
                                                        <span class="text-red-500">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-span-6">
                                                    {!! 
                                                        Form::label('message', __('messages.message') . ':', ['class' => 'block text-sm font-medium text-gray-700'])
                                                    !!}
                                                    <div class="mt-1">
                                                        {!! 
                                                            Form::textarea('message', null, [
                                                                'class' => 'shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md',
                                                                'autocomplete'=> "message",
                                                                'rows' => "3",
                                                                'placeholder' => __('messages.enter_your_message') . '...'
                                                                ]) 
                                                        !!}
                                                    </div>
                                                    @error('message')
                                                        <span class="text-red-500">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="px-4 py-3 sm:px-6">
                                            {!!
                                            Form::submit('Enviar comentario',[
                                                            'class' => 'inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500',
                                                            ])
                                            !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            @if (session('info'))
                            <span class="text-green-500">{{ session('info') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="flex">
                        <div class="ml-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2>
                                        <strong>{{ __('messages.Location') }}</strong>
                                    </h2>
                                </div>
                                <div class="card-body">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.595885394868!2d-58.49111494940723!3d-34.589090564134985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcb63f776d9989%3A0x3e813d3e0d15a9a7!2sCaracas%203707%2C%20C1419%20EJE%2C%20Buenos%20Aires!5e0!3m2!1ses!2sar!4v1613851389864!5m2!1ses!2sar"
                                        width="450" height="337" style="border:0;" allowfullscreen=""
                                        loading="lazy"></iframe>
                                </div>
                                <div class="card-footer">
                                    <div class="text-xs w-9/12 m-auto mt-4 p-5 border-2 border-gray-600">
                                        <p>
                                            <strong>Dirección:</strong>
                                            Caracas 3707 Piso 2
                                        </p>
                                        <p class="mt-2">
                                            <strong>Localidad:</strong>
                                            CABA
                                        </p>
                                        <p class="mt-2">
                                            <strong>Telefono:</strong>
                                            15-3356-4224
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </dl>
            </div>
        </div>
    </div>

</x-app-layout>
