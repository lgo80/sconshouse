<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">-->
    <title>Document</title>
</head>

<body>
    <h1>Consulta de un cliente de SconsHouse</h1>
    <p>La consulta la hizo:</p>
    <p><strong>Nombre: </strong>{{ $contacto['name'] }}</p>
    <p><strong>Email: </strong>{{ $contacto['email'] }}</p>
    <hr />
    <p>El mensaje es:</p>
    <p>{{ $contacto['message'] }}</p>
</body>

</html>
