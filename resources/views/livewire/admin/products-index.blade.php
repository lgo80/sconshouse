<div class="card">
    <div class="card-header">
        <input type="text" class="form-control" placeholder="Ingrese el titulo del post a buscar..."
            wire:model='search'>
    </div>
    @if ($products->count())
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            NAME
                        </th>
                        <th colspan="2">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            @can('admin.products.edit')
                                <td width='10px'>
                                    <a class="btn btn-primary btn-sm" href="{{ route('admin.products.edit', $product) }}">
                                        Editar
                                    </a>
                                </td>
                            @endcan
                            @can('admin.products.destroy')
                                <td width='10px'>
                                    <form action="{{ route('admin.products.destroy', $product) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm">Borrar</button>
                                    </form>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $products->links() }}
        </div>
    @else
        <div class="card-body">
            <strong>No se encontro ningun post relacionado con ese post.</strong>
        </div>
    @endif
</div>
