<div class="card">
    <div class="card-header">
        <input type="text" class="form-control" placeholder="Ingrese el titulo del post a buscar..."
            wire:model='search'>
    </div>
    @if ($promotions->count())
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            NAME
                        </th>
                        <th colspan="2">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($promotions as $promotion)
                        <tr>
                            <td>{{ $promotion->id }}</td>
                            <td>{{ $promotion->name }}</td>
                            @can('admin.promotions.edit')
                                <td width='10px'>
                                    <a class="btn btn-primary btn-sm"
                                        href="{{ route('admin.promotions.edit', $promotion) }}">
                                        Editar
                                    </a>
                                </td>
                            @endcan
                            @can('admin.promotions.destroy')
                                <td width='10px'>
                                    <form action="{{ route('admin.promotions.destroy', $promotion) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm">Borrar</button>
                                    </form>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $promotions->links() }}
        </div>
    @else
        <div class="card-body">
            <strong>No se encontro ningun post relacionado con ese post.</strong>
        </div>
    @endif
</div>
