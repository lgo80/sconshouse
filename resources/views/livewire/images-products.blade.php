<div class="card-columns">
    @foreach ($product->imageses as $image)
        <div class="card">
            <div class="image-lala">
                <img wire:click='destroy({{ $image }})' class="position-absolute"
                    src="{{ asset('images/cruz.png') }}" style="width: 25px; height: 25px; right:0px;">
            </div>
            <img class="card-img-top" id="picture{{ $image->id }}" src="{{ Storage::url($image->url) }}" alt="">
        </div>
    @endforeach
</div>
