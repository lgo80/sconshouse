<div>
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav class="bg-gray-800" x-data="{ open:false }">
        <div class="w-full mx-auto px-2 sm:px-6 lg:px-8">
            <div class="relative flex items-center justify-between h-16">
                <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <!-- Mobile menu button-->
                    <button x-on:click=' open = true '
                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                        aria-expanded="false">
                        <span class="sr-only">Open main menu</span>
                        <!-- Icon when menu is closed. -->
                        <!--
              Heroicon name: outline/menu
  
              Menu open: "hidden", Menu closed: "block"
            -->
                        <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                        <!-- Icon when menu is open. -->
                        <!--
              Heroicon name: outline/x
  
              Menu open: "block", Menu closed: "hidden"
            -->
                        <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>
                <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                    <div class="flex-shrink-0 flex items-center">
                        <a href='{{ route('home') }}' class="flex-shrink-0 flex items-center">
                            <img class="block h-12 w-auto" src="{{ asset('images/sconshouse.png') }}" alt="Home">
                            {{-- <img class="hidden lg:block h-8 w-auto"
                            src="https://tailwindui.com/img/logos/workflow-logo-indigo-500-mark-white-text.svg"
                            alt="Workflow"> --}}
                        </a>
                    </div>
                    <div class="hidden sm:block sm:ml-6">
                        <div class="flex space-x-4">
                            <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                            <a href="{{ route('home') }}"
                                class="{{ request()->routeIs('home') ? 'text-gray-800 bg-gray-500' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }} px-3 py-3 rounded-md text-sm font-medium">
                                {{ __('messages.home') }}
                            </a>
                            <a href="{{ route('we.index') }}"
                                class="{{ request()->routeIs('we.index') ? 'text-gray-800 bg-gray-500' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }} px-3 py-3 rounded-md text-sm font-medium">
                                {{ __('messages.we') }}
                            </a>
                            <a href="{{ route('product.index') }}"
                                class="{{ request()->routeIs('product.index') ? 'text-gray-800 bg-gray-500' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }} px-3 py-3 rounded-md text-sm font-medium">
                                {{ trans_choice('messages.products', 10) }}
                            </a>
                            <a href="{{ route('contact.create') }}"
                                class="{{ request()->routeIs('contact.create') ? 'text-gray-800 bg-gray-500' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }} px-3 py-3 rounded-md text-sm font-medium">
                                {{ __('messages.contact_us') }}
                            </a>
                        </div>
                    </div>
                </div>

                @auth
                    <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">

                        <!-- Profile dropdown -->
                        <div class="ml-3 relative" x-data="{ open:false }">
                            <div>
                                <button x-on:click=' open = true '
                                    class="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                                    id="user-menu" aria-haspopup="true">
                                    <span class="sr-only">Open user menu</span>
                                    <img class="h-8 w-8 rounded-full" src="{{ auth()->user()->profile_photo_url }}"
                                        alt="">
                                </button>
                            </div>
                            <!--
                                                          Profile dropdown panel, show/hide based on dropdown state.
                                              
                                                          Entering: "transition ease-out duration-100"
                                                            From: "transform opacity-0 scale-95"
                                                            To: "transform opacity-100 scale-100"
                                                          Leaving: "transition ease-in duration-75"
                                                            From: "transform opacity-100 scale-100"
                                                            To: "transform opacity-0 scale-95"
                                                        -->
                            <div x-show='open' x-on:click.away=' open = false '
                                class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5"
                                role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                                <a href="{{ route('profile.show') }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                                    {{ __('messages.profile') }}
                                </a>
                                @can('admin.home')
                                    <a href="{{ route('admin.home') }}"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                                        {{ __('messages.dashboard') }}
                                    </a>
                                @endcan
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a href="{{ route('logout') }}"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem"
                                        onclick="event.preventDefault();this.closest('form').submit();">
                                        {{ __('messages.log_out') }}
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                @else
                    <a href="{{ route('login') }}"
                        class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">
                        {{ __('messages.log_in') }}
                    </a>
                    <a href="{{ route('register') }}"
                        class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">
                        {{ __('messages.register') }}
                    </a>
                @endauth
            </div>
        </div>

        <!--
      Mobile menu, toggle classes based on menu state.
  
      Menu open: "block", Menu closed: "hidden"
    -->
        <div class="block sm:hidden" x-show="open" x-on:click.away=' open = false '>
            <div class="px-2 pt-2 pb-3 space-y-1">
                <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                <a href="{{ route('home') }}"
                    class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium">
                    {{ __('messages.home') }}
                </a>
                <a href="#"
                    class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                    {{ __('messages.we') }}
                </a>
                <a href="#"
                    class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                    {{ __('messages.products') }}
                </a>
                <a href="#"
                    class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">
                    {{ __('messages.contact_us') }}
                </a>
            </div>
        </div>
    </nav>
</div>
