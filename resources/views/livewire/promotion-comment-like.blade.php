<div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
    <span class="text-gray-600 inline-flex items-center leading-none text-2xl float-right -mt-14">
        <button wire:click='modifyILikePromotion({{ $promotion->id }},{{ count($promotion->liked()) }})'>

            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                class="w-4 h-4 mr-1 @if (count($promotion->liked())) {{ $colorTexto = 'text-indigo-500' }}
            @else
                {{ $colorTexto = 'text-black' }} @endif">
                <path
                    d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
            </svg>
        </button>
        <p><strong>{{ count($promotion->likes) }}</strong>
        </p>
    </span>
    <div class="sm:flex sm:items-start">
        <div class="w-full ml-10 mb-4 p-0">
            <div class="w-full bg-none">
                <label for="first_name" class="inline text-xs w-3/12 text-gray-700">
                    Dejar un comentario:
                </label>
                <input type="text" name="first_name" id="first_name" autocomplete="given-name"
                    placeholder="Ingrese un comentario..." wire:model='commentNew'
                    class="inline text-xs w-7/12 bg-gray-200 mt-1 mx-1 p-1 focus:ring-indigo-500 focus:border-indigo-500 focus:bg-white shadow-sm border-gray-600 rounded-md">
                <button type="submit" wire:click='saveComment({{ $promotion->id }})'
                    class="inline justify-center w-2/12 py-1 px-1 border border-transparent shadow-sm text-xs rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Enviar comentario
                </button>
            </div>
            @error('commentNew')
                <span class="error">
                    {{ $message }}
                </span>
            @enderror
        </div>
    </div>
    <div class="max-h-44 md:max-h-96 overflow-y-auto overflow-x-hidden">
        @foreach ($promotion->commentsOrderByIdDesc as $comment)
            <div class="pl-3">
                @php
                    $now = new DateTime('today');
                    $christmas = new DateTime($comment->created_at);
                    $interval = $now->diff($christmas);
                @endphp
                <p class="bg-gray-700 py-1 px-2 text-xs text-white">
                    {{ $comment->user->name . ' - Hace ' . $interval->format('%a días') }}
                </p>
                <p class="bg-gray-300 py-2 px-3 text-xs">
                    {{ $comment->comment }}
                </p>
                <span
                    class="text-gray-600 mr-3 inline-flex items-center lg:ml-auto md:ml-0 ml-auto leading-none text-sm px-3 pb-2 border-r-2 border-gray-300 bg-gray-300 w-full">
                    <button wire:click='modifyILikeComment({{ $comment }}, {{ $promotion->id }})'>

                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                            class="w-4 h-4 mr-1 @if (count($comment->liked())) {{ $colorTexto = 'text-indigo-500' }}
                        @else
                            {{ $colorTexto = 'text-black' }} @endif">
                            <path
                                d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                        </svg>
                    </button>
                    <p><strong>{{ count($comment->likes) }}</strong>
                    </p>
                </span>
            </div>
        @endforeach
    </div>
</div>
