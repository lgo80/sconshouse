<div class="ml-4 border-dotted border-4 border-gray-500 p-4">
    <aside>
        <h1 class="headerSecundary">
            {{ __('messages.my_delicacies_offert') }}
        </h1>
        <ul>
            @foreach ($promotions as $promotion)
                <li class="mb-0">
                    <div class="p-1 md:w-full">
                        <div class="h-full border-2 border-blue-400 rounded-lg overflow-hidden text-xs">
                            <img class="lg:h-36 md:h-24 w-full object-cover object-center"
                                src="{{ Storage::url($promotion->product->imageses->first()->url) }}" alt="blog">
                            <div class="p-3">
                                <h2 class="tracking-widest title-font font-medium text-gray-500">
                                    {{ trans_choice('messages.products', 1) }}
                                </h2>
                                <h1 class="title-font text-lg font-medium text-gray-900">
                                    {{ $promotion->name }}
                                </h1>
                                <p class="leading-relaxed mb-3 h-24">
                                    {{ $promotion->description }}
                                </p>
                                <p class="text-center my-2 pb-2 border-b-2 border-indigo-500">
                                    <strong>{{ __('messages.price') }}:</strong>
                                    ${{ $promotion->price }}.-
                                </p>
                                <div class="flex items-center flex-wrap m-0">
                                    <a class="text-indigo-500 inline-flex items-center mb-0"
                                        href="{{ route('promotions.show', $promotion) }}">
                                        {{ __('messages.learn_more') }}
                                        <svg class="w-4 h-4 ml-2 mb-0" viewBox="0 0 24 24" stroke="currentColor"
                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path d="M5 12h14"></path>
                                            <path d="M12 5l7 7-7 7"></path>
                                        </svg>
                                    </a>
                                    <span
                                        class="text-gray-600 mr-3 inline-flex items-center ml-auto leading-none text-sm pr-3 py-1 border-r-2 border-gray-300">
                                        <button
                                            wire:click='modifyILikePromotion({{ $promotion->id }},{{ count($promotion->liked()) }})'>

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                fill="currentColor" class="w-4 h-4 mr-1 @if (count($promotion->liked())) {{ $colorTexto = 'text-indigo-500' }}
                                            @else
                                                {{ $colorTexto = 'text-black' }} @endif">
                                                <path
                                                    d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                                            </svg>
                                        </button>
                                        <p><strong>{{ count($promotion->likes) }}</strong>
                                        </p>
                                    </span>
                                    <span class="text-gray-600 inline-flex items-center leading-none text-sm"
                                        x-data="{ open{{ $promotion->id }}:false }">
                                        <button x-on:click=' open{{ $promotion->id }} = true '>
                                            <svg class="w-4 h-4 mr-1 text-indigo-500" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round"
                                                stroke-linejoin="round" viewBox="0 0 24 24">
                                                <path
                                                    d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z">
                                                </path>
                                            </svg>
                                        </button>
                                        <p><strong>{{ count($promotion->comments) }}</strong></p>
                                        <div class="fixed z-10 inset-0 h-80" x-show='open{{ $promotion->id }}'>
                                            <div
                                                class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                                                <div class="fixed inset-0 transition-opacity" aria-hidden="true">
                                                    <div class="absolute inset-0 bg-gray-200 opacity-10"></div>
                                                </div>
                                                <span class="hidden sm:inline-block sm:align-middle sm:h-screen"
                                                    aria-hidden="true">&#8203;</span>
                                                <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                                                    role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                                                    <div class="bg-gray-200 px-4 py-3 sm:px-6 text-center">
                                                        <h3><strong>Comentarios</strong></h3>
                                                    </div>
                                                    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                                        <div class="sm:flex sm:items-start">
                                                            <div class="w-full ml-3 mb-4 p-0">
                                                                <div class="w-full bg-none mb-2">
                                                                    <label for="first_name"
                                                                        class="inline w-3/12 text-xs text-gray-700">
                                                                        Dejar un comentario:
                                                                    </label>
                                                                    <input type="text" name="first_name" id="first_name"
                                                                        autocomplete="given-name"
                                                                        placeholder="Ingrese un comentario..."
                                                                        wire:model='commentNew'
                                                                        class="inline text-xs bg-gray-200 mt-1 p-1 w-6/12 focus:ring-indigo-500 focus:border-indigo-500 focus:bg-white shadow-sm border-gray-600 rounded-md">
                                                                    <button type="submit"
                                                                        wire:click='saveComment({{ $promotion->id }})'
                                                                        class="inline justify-center py-1 px-1 border border-transparent shadow-sm text-xs rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                                        Enviar comentario
                                                                    </button>
                                                                </div>
                                                                @error('commentNew')
                                                                    <span class="error">
                                                                        {{ $message }}
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="max-h-44 md:max-h-96 overflow-y-auto overflow-x-hidden">
                                                            @foreach ($promotion->commentsOrderByIdDesc as $comment)
                                                                <div class="pl-3">
                                                                    @php
                                                                        $now = new DateTime('today');
                                                                        $christmas = new DateTime($comment->created_at);
                                                                        $interval = $now->diff($christmas);
                                                                    @endphp
                                                                    <p class="bg-gray-700 py-1 px-2 text-xs text-white">
                                                                        {{ $comment->user->name . ' - Hace ' . $interval->format('%a días') }}
                                                                    </p>
                                                                    <p class="bg-gray-300 py-2 px-3 text-xs">
                                                                        {{ $comment->comment }}
                                                                    </p>
                                                                    <span
                                                                        class="text-gray-600 mr-3 inline-flex items-center lg:ml-auto md:ml-0 ml-auto leading-none text-sm px-3 pb-2 border-r-2 border-gray-300 bg-gray-300 w-full">
                                                                        <button
                                                                            wire:click='modifyILikeComment({{ $comment }}, {{ $promotion->id }})'>

                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 20 20" fill="currentColor"
                                                                                class="w-4 h-4 mr-1 @if (count($comment->liked())) {{ $colorTexto = 'text-indigo-500' }}
                                                                            @else
                                                                                {{ $colorTexto = 'text-black' }} @endif">
                                                                                <path
                                                                                    d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                                                                            </svg>
                                                                        </button>
                                                                        <p><strong>{{ count($comment->likes) }}</strong>
                                                                        </p>
                                                                    </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="bg-gray-200 px-3 py-2 sm:px-6">
                                                        <button type="button"
                                                            x-on:click=' open{{ $promotion->id }} = false '
                                                            class="mt-3 w-full justify-center rounded-md border border-green-700 shadow-sm px-2 py-1 bg-green-300 text-xs text-gray-700 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </aside>
</div>
