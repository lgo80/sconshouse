<x-app-layout>
    <div class="body-primary">
        <div class="body-secundary">
            <div class="body-header-main">
                <h1 class="body-header-text">
                    {{ __('messages.my_delicacies') }}
                </h1>
            </div>
            <div class="Structure-idx-primary">
                <dl class="Structure-idx-body">
                    <div class="lg:col-span-4 xl:col-span-6">
                        @livewire('like-comment')
                    </div>
                    <div class="lg:col-span-2">
                        @livewire('special-offers',['promotions' => $promotions])
                    </div>
                </dl>
            </div>
        </div>
    </div>

</x-app-layout>
