<x-app-layout>
    <div class="body-primary">
        <div class="body-secundary">
            <div class="body-header-main">
                <h1 class="body-header-text">
                    {{ trans_choice('messages.products', 1) }}
                </h1>
            </div>
            <div class="Structure-shw-primary">
                <dl class="Structure-idx-body">
                    <div class="lg:col-span-4 xl:col-span-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="Structure-idx-header">{{ $product->name }}</h3>
                            </div>
                            <div class="card-body border-b-2 border-indigo-500">
                                <div class="grid grid-cols-1 gap-6">
                                    <div class="my-4">
                                        <img class="imgProduct"
                                            src="{{ Storage::url($product->imageses->first()->url) }}" alt="blog">
                                    </div>
                                    <div>
                                        <p class="pProduct">
                                            <strong>{{ $product->review }}</strong>
                                        </p>
                                    </div>
                                    <div>
                                        <p class="pProduct">
                                            {{ $product->description }}
                                        </p>
                                    </div>
                                    <div class="mt-6 pb-3">
                                        <p class="text-center text-2xl">
                                            <strong>{{ __('messages.price') }}:</strong>
                                            ${{ $product->price }}.-
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div>
                                    @livewire('comment-product-like',['product' => $product])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lg:col-span-2">
                        @livewire('special-offers',['promotions' => $product->promotionsByThree()])
                    </div>
                </dl>
            </div>
        </div>
    </div>
</x-app-layout>
