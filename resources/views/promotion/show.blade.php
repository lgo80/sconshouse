<x-app-layout>
    <div class="body-primary">
        <div class="body-secundary">
            <div class="body-header-main">
                <p class="body-header-text">
                    {{ trans_choice('messages.promotion', 1) }}
                </p>
            </div>
            <div class="Structure-shw-primary">
                <dl class="Structure-idx-body">
                    <div class="col-span-4 xl:col-span-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="Structure-idx-header">{{ $promotion->name }}</h3>
                            </div>
                            <div class="card-body">
                                <div class="grid grid-cols-1 gap-6">
                                    <div class="my-4">
                                        <img class="imgProduct"
                                            src="{{ Storage::url($promotion->product->imageses->first()->url) }}"
                                            alt="blog">
                                    </div>
                                    <div>
                                        <p class="pProduct">
                                            <strong>{{ $promotion->review }}</strong>
                                        </p>
                                    </div>
                                    <div>
                                        <p class="pProduct">
                                            {{ $promotion->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="mt-6 pb-3 border-b-2 border-indigo-500">
                                    <p class="text-center text-2xl">
                                        <strong>{{ __('messages.price') }}:</strong>
                                        ${{ $promotion->price }}.-
                                    </p>
                                </div>
                                <div>
                                    @livewire('promotion-comment-like',['promotion' => $promotion])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-2">
                        @livewire('special-offers',['promotions' => $promotion->product->promotionsByThree()])
                    </div>
                </dl>
            </div>
        </div>
    </div>

</x-app-layout>
