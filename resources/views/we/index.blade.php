<x-app-layout>
    <div class="body-primary">
        <div class="body-secundary xl:px-96">
            <div class="body-header-main">
                <p class="body-header-text">
                    {{ __('messages.we') }}
                </p>
            </div>

            <div class="Structure-shw-primary">
                <dl class="nivel5">
                    <div class="flex">
                        <div class="ml-4">
                            <img class="mt-10 ring-4 ring-gray-400" src="images/Foto_nosotros.jpg" alt="">
                        </div>
                    </div>

                    <div class="flex">
                        <div class="ml-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2>
                                        <strong>SconsHouse</strong>
                                    </h2>
                                </div>
                                <div class="card-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque posuere sit
                                        amet dolor id egestas. Praesent rutrum congue ornare. Mauris id malesuada urna.
                                        Vestibulum bibendum, enim fermentum fringilla varius, sem elit tincidunt massa,
                                        vitae pulvinar orci velit sit amet felis. Etiam ullamcorper lobortis magna.
                                        Maecenas velit felis, iaculis sed ultrices id, auctor ac diam. Vestibulum in
                                        venenatis felis. Aenean commodo ipsum vitae imperdiet consequat. Pellentesque
                                        habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                        Quisque congue odio sagittis nisi tincidunt, sed suscipit enim ornare. Donec
                                        faucibus vestibulum nibh, sit amet porttitor urna hendrerit vitae. Aenean
                                        aliquam, mi a egestas sollicitudin, sapien quam molestie dui, sed sollicitudin
                                        ante leo id nulla. Vivamus nec orci ullamcorper enim luctus sodales.
                                    </p>
                                    <p>
                                        Fusce egestas, ligula at feugiat vestibulum, odio turpis vestibulum sem, ut
                                        pulvinar nulla tortor eu lacus. Aliquam quam diam, venenatis non elementum in,
                                        scelerisque eget purus. Nullam congue a velit maximus tempor. Donec vestibulum
                                        eros bibendum, accumsan risus nec, dictum libero. Proin a metus porttitor,
                                        pretium metus vel, ultrices nulla. Ut purus ipsum, hendrerit rhoncus justo
                                        tristique, tincidunt cursus urna. In quis dui nulla. Sed efficitur sapien ut
                                        molestie efficitur. Morbi id auctor dui. Integer ornare elementum enim a
                                        dapibus.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </dl>
            </div>
        </div>
    </div>
</x-app-layout>
