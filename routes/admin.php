<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\ProductControlador;
use App\Http\Controllers\Admin\PromotionControlador;
use App\Http\Controllers\Admin\UserControlador;
use Illuminate\Support\Facades\Route;

Route::get('', [HomeController::class, 'index'])
    ->middleware('can:admin.home')
    ->name('admin.home');
Route::resource('products', ProductControlador::class)
    ->except('show')
    ->names('admin.products');
Route::resource('promotions', PromotionControlador::class)
    ->except('show')
    ->names('admin.promotions');
Route::resource('users', UserControlador::class)
    ->only(['index', 'edit', 'update'])
    ->names('admin.users');
Route::resource('images', ImageController::class)
    ->only(['store','destroy'])
    ->names('admin.images');
